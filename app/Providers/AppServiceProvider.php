<?php

namespace App\Providers;
use App\Service\Mystifly\{FareRules,AirRevalidate};
use Illuminate\Support\ServiceProvider;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        \URL::forceScheme('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('FareRules',FareRules::class);
        $this->app->bind('AirRevalidate',AirRevalidate::class);
        $this->app->bind('PathHelper', function($app)
        {
            return new  class {
                
                private $domain = false;

                public function getSchema()
                {
                    return config('constants.DefaultSchema');
                }

                public function getDomain()
                {
                    if ($this->domain === false) {
                        $this->domain = Request::getHost();
                    }
                    return $this->domain;
                }

                public function getApiPath()
                {
                    if($this->getDomain() == 'demo.adventuretravel.com.au'){
                        return $this->getSchema()."cms.adventuretravel.com.au/api/";
                    }
                    return $this->getSchema().config('constants.DefaultApiPath');
                }

                public function getDomainPath()
                {
                     if($this->getDomain() == 'demo.adventuretravel.com.au'){
                        return $this->getSchema()."cms.adventuretravel.com.au/";
                    }
                    return $this->getSchema().config('constants.DefaultDomainPath');
                }

                public function getMapApi()
                {
                     if($this->getDomain() == 'demo.adventuretravel.com.au'){
                        return $this->getSchema()."cms.adventuretravel.com.au/api/map/";
                    }
                    return $this->getSchema().config('constants.DefaultMapApiPath');
                }

               
            };
        });
    }
}
