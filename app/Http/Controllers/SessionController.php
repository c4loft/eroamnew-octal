<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\EroamSession;

class SessionController extends Controller
{
	private $session;

	public function __construct() {
		$this->session = new EroamSession;
	}

	public function set_currency() {
		$currency = request()->input( 'currency' );
		$currency_id = request()->input( 'id' );
		return $this->session->set_currency( $currency, $currency_id );
	}
	public function set_city() {
		$default_selected_city = request()->input( 'default_selected_city' );
		$showCityModal = request()->input( 'showCityModal' );
		return $this->session->set_city( $default_selected_city, $showCityModal );
	}
	public function get_currency() {
		return  $this->session->get_currency( 'currency' );
	}

	public function get_search_session() {
		return $this->session->get_search();
	}

	public function set_transport_filter(){
		$filter     = request()->input('filter');
		$day        = request()->input('day');
		$fl_options = request()->input('flight_options');
		$leg        = request()->input('leg');
		return $this->session->set_transport_filter($filter, $day, [ intval( $leg ) => $fl_options ]);
	}

	public function set_travel_preferences(){
		$travel_preferences = request()->input('travel_preference');
		return $this->session->set_travel_pref($travel_preferences);
	}

	public function update_dates_available(){
		$id = request()->input('id');
		$dates_available = request()->input('dates_available');
		return $this->session->update_dates_available($dates_available, $id);
	}

	public function set_flight_options(){
		
		return $this->session->set_flight_options( $data );
	}

	public function get_search(){
		return json_encode( session()->get( 'search' ) );
	}

	public function set_initial_session(){
		return $this->session->initial_session();
	}

	/* Added By Rekha Patel - Tour Country Checkbox Data - 25th Oct 2017*/
	public function set_tourCountry(){
		$tourCountry = request()->input('tourCountryData');
		return $this->session->set_tourCountry_session($tourCountry);
	}
	
	public function set_tourHome(){
		$fromTourHome = request()->input('fromTourHome');
		return $this->session->set_tourHome_session($fromTourHome);
	}
}
