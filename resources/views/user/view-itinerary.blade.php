@extends('layouts.home')

@section('custom-css')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<style type="text/css">
	.itinerary-container {
		padding: 2rem;
		min-height: 600px;
	}
	.itinerary-header {
		margin-bottom: 3.5rem;
	}
	.itinerary-legs {
		padding: 1rem;
	}
	.itinerary-leg {
		outline: none;
	}
	.city-description p {
		font-weight: 300;
		font-size: 15px;
	}
	.itinerary-leg .city-image {
		/*height: 300px;*/
	}
	.itinerary-leg-row {
		margin-bottom: 5rem;
	}
	.activity-row {
		margin-bottom: 2.5rem;
	}
	.itinerary-leg img {
		border: 1px solid rgba(0, 0, 0, 0.1);
	}
	.leg-item {
		margin-bottom: 3rem;
	}
	#city-markers {
		margin: 4rem 0;
	}
	#city-markers ul {
		padding: 0;
		margin: 0;
	}
	#city-markers ul li {
		display: inline-block;
		margin: 1rem;
	}
	#city-markers ul li button {
		background: none;
		height: auto;
		width: auto;
		border: none;
		font-size: 13px;
		outline: none;
		padding: 0;
		padding-top: 1rem;
	}
	#city-markers ul li button:before {
		content: '\f041';
		font-family: 'FontAwesome';
		font-size: 2.5rem;
		display: inline-block;
		width: 100%;
	}
	#city-markers ul li.slick-active button:before {
		color: #2AA9DF;
	}
	.slick-slider {
		-webkit-user-select: text;
		-khtml-user-select: text;
		-moz-user-select: text;
		-ms-user-select: text;
		user-select: text;
	}
</style>
@stop

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 white-box itinerary-container">
				<div class="row">
					<div class="col-xs-12 text-center itinerary-header">
						<h4 class="uppercase bold-txt blue-txt">{{ $itinerary->title }} - {{ $itinerary->currency.' $'.$itinerary->total_amount }}</h4>
						<h4 class="grey-txt">{{ sing($itinerary->total_nights, 'nights', true) }} from {{ $itinerary->currency.' '.$itinerary->total_per_person }} (per person)</h4>
						<h5>{{ date('jS, F Y', strtotime($itinerary->travel_date)) }}</h5>
					</div>
					<div class="text-center" id="city-markers"></div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1 itinerary-legs">
						@foreach ($itinerary->legs as $key => $leg)
							<div class="itinerary-leg">
								<div class="row itinerary-leg-row">
									<div class="col-xs-12">
										<h4 class="uppercase"><span class="flaticon-skyline"></span> {{ $leg->city->name }}</h4>
									</div>
									<div class="col-md-7">
										<div class="city-description">{!! $leg->city->description !!}</div>
									</div>
									<div class="col-md-5">
										<img src="{{ config('env.CMS_URL').$leg->city->image[0]->small }}" class="img-responsive">
									</div>
								</div>
								<div class="row itinerary-leg-row">
									<div class="col-xs-12">
										<h4 class="uppercase"><span class="flaticon-rest"></span> Accomodation</h4>
									</div>
									<div class="col-xs-12">
										<span>{{ $leg->hotel->name }} ({{ $leg->hotel->room_type }})</span><br>
										<span class="bold-txt blue-txt">{{ $itinerary->currency.' $'.$leg->hotel->price }}</span><br>
										<h5 class="uppercase">Description</h5>
										<p class="light-txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. In facere temporibus provident id voluptatibus, asperiores, esse velit dolores magni voluptates! Nobis voluptatem rerum veniam nulla eius dolores mollitia. Similique, facilis?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa fugit illo voluptates autem deserunt alias velit placeat quo ducimus debitis molestias perferendis quibusdam explicabo laborum nihil, earum ab quasi unde.</p>
										<h5 class="uppercase">Cancellation Policy</h5>
										<p class="light-txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque sunt consequatur modi, voluptatum, iure, eius id impedit recusandae aliquam voluptate sit illum aliquid laboriosam non suscipit perferendis? Hic, quas, nostrum.</p>
									</div>
									{{-- <div class="col-md-4">
										<img src="{{ $leg->hotel->image }}" class="img-responsive">
									</div> --}}
								</div>
								@if ($leg->activities)
									<div class="row itinerary-leg-row">
										<div class="col-xs-12">
											<h4 class="uppercase"><span class="flaticon-sports"></span> Activities</h4>
										</div>
										@foreach ($leg->activities as $activity)
											<div class="col-xs-12 activity-row">
												<span>{{ $activity->name }}</span><br>
												<span class="bold-txt blue-txt">{{ $itinerary->currency.' '.$activity->price }}</span><br>
												<h5 class="uppercase">Description</h5>
												<p class="light-txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. In facere temporibus provident id voluptatibus, asperiores, esse velit dolores magni voluptates! Nobis voluptatem rerum veniam nulla eius dolores mollitia. Similique, facilis?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa fugit illo voluptates autem deserunt alias velit placeat quo ducimus debitis molestias perferendis quibusdam explicabo laborum nihil, earum ab quasi unde.</p>
											</div>
											{{-- <div class="col-md-4">
												<img src="{{ $activity->image }}" class="img-responsive">
											</div> --}}
										@endforeach
									</div>
								@endif
								@if ($leg->transport)
									<div class="row itinerary-leg-row">
										<div class="col-xs-12">
											<h4 class="uppercase"><span class="flaticon-plane"></span> Onward Transport</h4>
											<h4>To {{ $leg->transport->destination }} via {{ $leg->transport->transport_type }} <span class="bold-txt blue-txt">{{ $itinerary->currency.' '.$leg->transport->price }}</span></h4>
										</div>
									</div>
								@elseif (last($itinerary->legs) != $leg)
									<div class="row itinerary-leg-row">
										<div class="col-xs-12">
											<h4 class="uppercase"><span class="flaticon-plane"></span> Onward Transport</h4>
											<h4>To {{ $itinerary->legs[$key + 1]->city->name }} <span class="bold-txt blue-txt">Own Arrangement</span></h4>
										</div>
									</div>
								@endif
							</div>
						@endforeach
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<input type="hidden" id="itinerary-legs" value="{{ json_encode($itinerary->legs) }}">
@stop

@section('custom-js')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.itinerary-legs').slick({
			infinite: false,
			dots: true,
			nextArrow: '',
			prevArrow: '',
			appendDots: $('#city-markers'),
			draggable: false,
		});

		var itineraryLegs = JSON.parse($('#itinerary-legs').val());
		console.log(itineraryLegs);
		$.each(itineraryLegs, function(key, value) {
			$('#city-markers').find('ul li button').eq(key).html(value.city.name);
		});
	});
</script>
@stop