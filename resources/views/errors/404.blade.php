@extends('layouts.common')
@push('style')
<style type="text/css">
	body {
		background-image: url('{{ url("images/tour-surf-lifestyle2.jpg") }}');
		background-size: cover;
	}
	.content p{
		color: #fff;
		font-size: 20px;
	}
	.content h1, .content h2 {
		color: #fff;
	}
	.content {
		margin-top: 100px;
	}
	.content a {
		color: #fff;
		text-decoration: underline;
	}
</style>
@endpush
@section('content')
	<div class="content text-center">
		<img src="{{ url('images/logo.png') }}" alt="" class="img-responsive error-logo" />
		<h1 class="mt-5"><span>4</span><span class="letter-second">0</span><span>4</span></h1>
		<h2 class="mt-3">Oops!</h2>
		<p>we can't seem to find the page you're looking for.</p>
		<p class="mt-5">Go to <a href="/">Home</a></p>
	</div>
@stop