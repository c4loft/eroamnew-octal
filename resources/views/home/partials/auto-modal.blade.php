<div id="myModal" class="modal fade bd-auto-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer">
                <div class="modal-header">
                    <h4>Tailor-Made Auto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <i class="ic-clear"></i>
     </button>
                </div>
                <div class="modalform">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ url('images/auto-map.jpg') }}">
                        </div>
                        <div class="col-md-8">
                            <p>
                                Tailor-Made Auto will automatically generate a multi-city itinerary between your two selected locations. The itinerary will include multiple ‘trending’ cities, recommended tours, transport and hotels. Once this itinerary has been created, you have the ability to edit any segment using the interactive map, or booking summary panel.
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>