<script type="text/javascript">
	var countryName   = '{{ $countryName }}';
	var default_currency   = '{{ $default_currency }}';
	var siteUrl = $( '#site-url' ).val();
	var activitySequence = ['{{ join(', ', $interest_ids) }}'];
	
	var project_search_cnt = 1;
	var end_location_search_cnt = 1;
	var start_location_search_cnt = 1;

	$(document).ready(function(){
		tourList();
	});

	function tourList(next){
	  tour_data = {
	    countryName: countryName,
	    provider: 'getTours',
	    default_selected_city: '<?php echo session()->get( 'default_selected_city' );?>'
	  };


	  // CACHING HOTEL BEDS
	  var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'getTours', true);
	  
	  eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
	   if( tourResponse.length > 0 ){
	      $.each( tourResponse, function( key, value ) {
	        appendTours( value );
	      })

	    }
	  })
	}
	var cnt1 = 1;
	function getStars(count, half = false){
	  var stars = '';
	  if( parseInt( count ) ){
	    for( star = 1; star <= count; star ++ ){
	      stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
	    }
	    var emptyStars = 5 - parseInt( count );
	    if(half){
	      stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
	      emptyStars = emptyStars - 1;
	    }
	    for( empty = 1; empty <= emptyStars; empty ++ ){
	      stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
	    }
	  }else{
	    for( star = 1; star <= 5; star ++ ){
	      stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
	    }
	  }
	   var stars = '';
	  return stars;
	}

	function appendTours(tour){
	    $("#tours-loader").hide();
	    if(cnt1 == 22){return false;}
	    var price = 0;
	    var land_price = 0;
	    if(!tour.flightPrice  || tour.flightPrice < 1){
	      tour.flightPrice = 0;
	    }
	    if(tour.flightDepart != '' && tour.flightDepart != null){
	      <?php 
	        $default_selected_city = session()->get( 'default_selected_city' );
	        $city_name = 'MEL';
	        $city_full_name = 'Melbourne';
	        if($default_selected_city == 7){
	          $city_name = 'SYD';
	          $city_full_name = 'Sydney';
	        }elseif($default_selected_city == 15){
	          $city_name = 'BNE';
	          $city_full_name = 'Brisbane';
	        }elseif($default_selected_city == 30){
	          $city_name = 'MEL';
	          $city_full_name = 'Melbourne';
	        }
	      ?>
	      tour.flightDepart = '<?php echo $city_name;?>';
	      var city_full_name = '<?php echo $city_full_name;?>';
	    }
	    if(tour.price){
	      price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
	      land_price = parseFloat(tour.price).toFixed(2);
	    }
	    
	    var star;
	    var rating = tour.rating;

	    if(rating % 1 === 0){
	        stars = getStars( rating);
	    }else {
	        stars = getStars( rating, true );
	    }
        var aws_forlder_path  = (tour.folder_path).slice(1);

	  // var imgurl = 'http://www.adventuretravel.com.au'+tour.folder_path+'245x169/'+tour.thumb;
      //var imgurl = 'http://www.adventuretravel.com.au/'+tour.folder_path+'/OriginalImage/'+tour.thumb;
      var imgurl =  $('meta[name="aws_url"]').attr('content')+ aws_forlder_path +  tour.thumb;
	  var imgurl2 = 'http://dev.cms.eroam.com/'+tour.thumb;

	  $("#overlay").hide();
	    if(parseInt(tour.no_of_days) == 1) {
	        var day = 'Day';
	    }else{
	        var day = 'Days';
	    }
	    var str1 = tour.short_description;
	    if(str1.length > 120) str1 = str1.substring(0,120);
	    
        var maxLength = 120 // maximum number of characters to extract

        //trim the string to the maximum length
        var trimmedString = str1.substr(0, maxLength);

        //re-trim if we are in the middle of a word
        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))

	    var total_duration = ''; 
	    var total_duration1 = ''; 
	    if(tour.durationType == 'd'){
	      total_duration = 'DAY';
	      total_duration1 = 'Day';
	      if(Math.ceil(tour.no_of_days) > 1){
	        total_duration = 'DAYS';
	        total_duration1 = 'Days';
	      }
	    }else if(tour.durationType == 'h'){
	      total_duration = 'HOUR';
	      total_duration1 = 'Hour';
	      if(Math.ceil(tour.no_of_days) > 1){
	        total_duration = 'HOURS';
	        total_duration1 = 'Hours';
	      }
	    } 
	    if(tour.start_date){
	      tour.start_date = formatDate(new Date(tour.start_date),"start");
	    }else{
	      tour.start_date = '';
	    }
	    if(tour.end_date){
	      tour.end_date = formatDate(new Date(tour.end_date),"end");
	    }else{
	      tour.end_date = '';
	    }
	    if(!tour.discount || tour.discount == '.00'){
	      tour.discount = 0;
	    }
	    if(!tour.saving_per_person || tour.saving_per_person == '.00'){
	      tour.saving_per_person = '0.00';
	    }
	    if(!tour.retailcost){
	      tour.retailcost = '0.00';
	    }
		
		var no_of_days = Number(tour.no_of_days);
		var no_of_days_str = '';
        var day_str    = '';
		if(no_of_days > 1){
			no_of_days_str = no_of_days+'<span>Days</span>';
            day_str = 'Days';
		}else{
			no_of_days_str = no_of_days+'<span>Day</span>';
            day_str = 'Day';
		}
		
	    var html = '<a href="<?php echo url("tourDetail/"); ?>/'+tour.tour_id+'/'+tour.tour_url+'"><div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">'+
                            '<div class="flipping_box top_tour_sec">'+
                                '<div class="flipper">'+
                                    '<div class="front">'+
                                        '<div class="tourdetails">'+
                                            '<div class="plan_image">'+
                                                '<img src="'+imgurl+'" alt="eRoam" />';
                                                if(tour.flightPrice != 0){ 
                                                	html+='<div class="tourby">'+
	                                                    '<div class="tourby_icon bus"><i class="ic-directions_bus"></i></div>'+
	                                                    '<div class="tourby_icon car"><i class="ic-direction_car"></i></div>'+
	                                                    '<div class="tourby_icon airliance"><i class="ic-flight"></i></div>'+
                                                	'</div>';
                                               }
                                            html+='</div>'+
                                            '<div class="toru_description d-flex">'+
                                                '<div class="toru_name d-flex align-items-end flex-column justify-content-start">'+
                                                    '<h4 class="w-100">'+ tour.tour_title +'</h4>'+
                                                    '<p class="more w-100">'+ tour.tripCountries +'</p>'+
                                                    '<p class="dis mt-auto mb-3" style="padding:10px 0px 0px 0px !important">'+trimmedString + '...'+'</p>'+
                                                '</div>'+
                                                '<div class="toru_price">'+
                                                    '<div class="dayes">'+ no_of_days_str+'</div>'+
                                                    '<div class="price">$'+parseInt(price)+'</div>'+
                                                    '<small><i>Usually '+Math.round(tour.retailcost).toFixed(2)+'</i></small>';
                                                    if(tour.flightPrice != 0){ 
                                                        html+='<p class="inclusive">Flights Inclusive*</p>';
                                                    }
                                                    else
                                                    {
                                                        html+='<p class="inclusive">&nbsp</p>';
                                                    }
                                                html+='</div>'+
                                                '<div class="clearfix"></div>'+
                                            '</div>'+
                                            '<div class="offer_availabel">'+
                                                '<p>OFFER AVAILABLE FOR</p>'+
                                                '<ul>'+
                                                    '<li>'+
                                                        '02'+
                                                        '<p>DAYS</p>'+
                                                    '</li>'+
                                                    '<li>'+
                                                        '10'+
                                                        '<p>HOURS</p>'+
                                                    '</li>'+
                                                    '<li>'+
                                                        '30'+
                                                        '<p>MINS</p>'+
                                                    '</li>'+
                                                    '<li>'+
                                                        '37'+
                                                        '<p>SECS</p>'+
                                                    '</li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="clearfix"></div>'+
                                    '</div>'+
                                    '<div class="back">'+
                                        '<div class="flipp_back">'+
                                            '<h4>'+tour.tour_title+'</h4>'+
                                            '<p class="more">'+tour.tripCountries+'</p>'+
                                            '<div class="price">$'+price+' </div>';
                                            if(tour.flightPrice != 0){
                                                html +='<em class="alertmsg">Per Person. Flights inclusive departing MEL / SYD / BNE</em>';
                                            }
                                            html +='<div class="tourdetails">'+
                                                '<ul>';
                                                var land_only = '';
                                                if(tour.flightPrice != 0){
                                                  land_only = '(Land Only)';
                                                  html +='<li><span>Land Only</span> <strong>'+ land_price +'</strong> Per Person</li>';
                                                }
                                                html+='<li><span>Savings From</span> <strong>'+ ( parseInt(price) - parseInt(land_price)) +'</strong> Per Person</li>'+
                                                '</ul>'+
                                            '</div>'+
                                            '<h4 class="titledetails">Tour Details</h4>'+
                                            '<div class="tourdetails block_2">'+
                                                '<ul>'+
                                                    '<li><span>Travel Style</span> '+tour.category_name+'</li>'+
                                                    '<li><span>Tour Length</span> '+ tour.no_of_days+ ' '+day_str+' </li>'+
                                                    '<li><span>Tour Dates</span> 1 Jan 2018 - 31 Dec 2018</li>'+
                                                    '<li><span>Start / Finish</span> '+tour.departure+' - '+tour.destination+'</li>'+
                                                '</ul>'+
                                            '</div>'+
                                            '<em class="alertmsgbottom">*From price, discount and savings per person is based on the total price per adult in a twin share room, subject to departure dates.</em>'+
                                        '</div>'+
                                        '<div class="viewoffer_outer">'+
                                            '<a href="<?php echo url("tourDetail/"); ?>/'+tour.tour_id+'/'+tour.tour_url+'" class="viewoffer" target="_blank">view offer</a>'+
                                        '</div>'+
                                        '<div class="clearfix"></div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="clearfix"></div>'+
                            '</div>'+
                        '</div></a>';

	    
	    $('#tourList').append(html);
	    $("#tourList").find('.flipper').css('height', $(".tourdetails").outerHeight());
	    cnt1 = parseInt(cnt1) + 1;
	    
	}


	$(document).on('click','.viewoffer_outer',function() {
        var url = $(this).find('a').attr('href');
        window.open(url, '_blank');
    });

	 $('body').on('click', '#save_travel_preferences', function() { 
            var btn = $(this).text('loading..');
            if($('.nationality_id').val()){
              var nationality_dom = $(".nationality option:selected").text();
            }else{
              var nationality_dom = $(".nationality option:selected").val();
            }
            

            if($('.age_group').val()){
              var age_group_dom = $(".age_group option:selected").text();
            }else{
              var age_group_dom = $(".age_group option:selected").val();
            }
            var age_group_id = $('.age_group').val();
            var cabin_class = $('.CabinClasses').val();
            var nationality_id = $('.nationality').val();
            var gender = $('.gender').val();
            var nationality = ( isNotUndefined( nationality_dom ) ) ? nationality_dom : [] ;
            var age_group = ( isNotUndefined( age_group_dom ) ) ? age_group_dom : [] ;
            var interestLists = [];
            var accommodations = [];
            var accommodationIds = [];
            var interestListIds = [];
            var roomTypeIds = [];
            var roomTypes = [];
            var transportTypeIds = [];
            var transportTypes = [];
              
            $('.hotel_category option:selected').each(function(){
              if($(this).val()){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                accommodationIds.push(id);
                accommodations.push(name);
              }
            });

            $('.room_type option:selected').each(function(){
              var id = parseInt($(this).val());
              var name = $(this).attr('data-name');
              roomTypeIds.push(id);
              roomTypes.push(name);
            });
            
            $('.transport_type_options option:selected').each(function(){
              if($(this).val()){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                transportTypeIds.push(id);
                transportTypes.push(name);
              }
            });
            
            $('.interest-button-active').each(function(){
                interestLists.push($(this).attr('data-name'));
            });
			
            var  data = { 
              travel_preference: [{
                cabin_class:cabin_class, 
                accommodation:accommodationIds, 
                accommodation_name: accommodations,
                room_name: roomTypes,
                room: roomTypeIds,
                transport_name: transportTypes,
                transport: transportTypeIds,
                age_group: age_group,
                nationality: nationality,
                gender: gender,
                interestLists: interestLists.join(', '),
                interestListIds: activitySequence,
                //interestListIds: interestListIds
              }]
            };
            

            var interestText = interestLists.length > 0 ? interestLists.join(', ') : 'All';
            var accommodation = accommodations.length > 0 ? accommodations.join(', ') : 'All';
            var transport = transportTypes.length > 0 ? transportTypes.join(', ') : 'All';
            var nationalityText =  nationality !=''  ? nationality : 'All'; 
          
            var ageGroupText =  age_group !='' ? age_group : 'All'; 

            $('#_accommodation').html(' <strong> Accommodation: </strong> '+accommodation);
            $('#_transport').html(' <strong> Transport: </strong> '+transport);
            $('#_nationality').html(' <strong> Nationality: </strong> '+nationalityText);
            $('#_age').html(' <strong> Age: </strong> '+ageGroupText);
            $('#_interests').html(' <strong> Interests: </strong> '+ interestText);
            

            @if (session()->has('user_auth'))
              var post = {
                cabin_class:cabin_class, 
                hotel_categories:accommodationIds, 
                hotel_room_types: roomTypeIds,
                transport_types: transportTypeIds,
                age_group: age_group_id,
                nationality: nationality_id,
                gender: gender,
                interests: activitySequence,
                _token: $('meta[name="csrf-token"]').attr('content'),
                user_id: "{{ @session()->get('user_auth')['user_id'] }}",
              };
              eroam.ajax('post', 'save/travel-preferences', post, function(response){
                
              });
            @endif;

            eroam.ajax('post', 'session/travel-preferences', data, function(response){
              setTimeout(function() {
				btn.text('Accept');
                btn.button('reset');
				
                eroam.ajax('get', 'session/updatePreferences', '', function(responsedata){
                  $("#changePreferences").html(responsedata);
                });
                 body.css('overflow','auto');
                $('#preferencesModal').modal('hide');
              }, 5000);
            });
          });


	function formatDate(date,type) {
	    var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
	    var day = date.getDate();
	    var monthIndex = date.getMonth();
	    var year = date.getFullYear();
	    if(type == 'start'){
	        return day + ' ' + monthNames[monthIndex];
	    }else{
	        return day + ' ' + monthNames[monthIndex] + ' ' + year;
	    }
	}
</script>
<script>
	var body = $('body');
	var modal = $('#preferencesModal');

	function showModal() {
		body.css('overflow','hidden');
		modal.show().css('overflow', 'auto');
	}

	function hideModal() {
	   body.css('overflow','auto');
	   $('body').css('overflow','auto')
	   modal.hide();
	}

	$('.profile_popup').on('click', showModal);
    $('.close').on('click', hideModal);
	$('.cancel_custom_pop_up').click(function(){
        $('body').css('overflow','auto');
    });


</script>
