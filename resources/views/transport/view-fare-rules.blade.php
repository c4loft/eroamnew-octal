@if(!empty($category))
<div class="row vertical-tab">	
	<div class="col-sm-4">
		<div class="baggage-left-content">
	    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
	    @foreach($category as $key => $cat)
		    @if(!empty($cat))
			    @if($key == 0)
			    	<a class="nav-link active" id="v-pills-home-tab{{$key}}" data-toggle="pill" href="#v-pills-home{{$key}}" role="tab" aria-controls="v-pills-home{{$key}}" aria-selected="true">Baggage</a>
				@else
					<a class="nav-link" id="v-pills-home-tab{{$key}}" data-toggle="pill" href="#v-pills-home{{$key}}" role="tab" aria-controls="v-pills-home{{$key}}" aria-selected="true">{{strip_tags(ucwords(strtolower($cat)))}}</a>
				@endif
			@endif
		@endforeach
	    </div>
	    </div>
    </div>
	<div class="col-sm-8">
	    <div class="tab-content baggage-content" id="v-pills-tabContent">
	    	@foreach($rules as $key => $rule)
	    	
	    	@if(!empty($rule))
			    @if($key == 0)

			    <div class="tab-pane fade show active" id="v-pills-home{{$key}}" role="tabpanel" aria-labelledby="v-pills-home-tab{{$key}}">

			    	@foreach($baggage as $keyBaggage => $valBaggage)
			    		<div class="row mb-2">
			    			<div class="col-sm-4">
			    				<strong>Departure - Arrival</strong><br>
			    				{{$valBaggage['Departure'].' - '.$valBaggage['Arrival']}}
			    			</div>
			    			<div class="col-sm-4">
			    				<strong>Flight No</strong><br>
			    				{{$valBaggage['FlightNo']}}
			    			</div>
			    			<div class="col-sm-4">
			    				<strong>Baggage Info</strong><br>
			    				{{$valBaggage['Baggage']}}
			    			</div>
			    		</div>
			    		<hr>
			    	@endforeach

			    </div>

			    @else
			    	<div class="tab-pane fade show" id="v-pills-home{{$key}}" role="tabpanel" aria-labelledby="v-pills-home-tab{{$key}}">
			           {!! nl2br(strip_tags(ucwords(strtolower($rule)))) !!}
			        </div>
			    @endif    
	        @endif
			@endforeach
	    </div>
	</div>
</div>	
@else
<p class="text-center mt-3">No Fare Rules Available.</p>
@endif
	


                        
                    