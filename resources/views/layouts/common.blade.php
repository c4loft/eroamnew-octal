@include('layouts.partials.header')
<section class="content-wrapper">
	@yield('content' )
</section>
@include('layouts.partials.footer')