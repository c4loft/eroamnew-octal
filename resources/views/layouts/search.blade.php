@include( 'layouts.partials.search_header' )
	<div class="clearfix"></div>
	<div class="page-container">
		{{-- @if ( request()->segment( 1 ) == 'map' )
			<form action="{{ url( 'search' ) }}" method="post">
				<div class="white-box row margin-btm">
					<div class="col-xs-12">
						<div class="row content-header no-padding-btm">
							<div class="option-container col-xs-6">
								<a href="#" data-type="manual" class="search-option-btn padding {{ session()->get( 'search_input' )['option'] == 'manual' ? 'active' : '' }}">Multi-City Manual</a>
								<input class="search-option" type="radio" id="option1" name="option" value="manual" {{ session()->get( 'search_input' )['option'] == 'manual' ? 'checked' : '' }}>
							</div>
							<div class="option-container col-xs-6">
								<a href="#" data-type="auto" class="search-option-btn padding {{ session()->get( 'search_input' )['option'] == 'auto' ? 'active' : '' }}">Multi-City Auto</a>
								<input class="search-option" type="radio" id="option2" name="option" value="auto" {{ session()->get( 'search_input' )['option'] == 'auto' ? 'checked' : '' }}>
							</div>
						</div>
						<div class="row content-header">
							<div class="col-md-2">
								<select id="country" name="country" class="select-country">
									<option value="#">Starting Country</option>
									@foreach ( $countries as $country )
										@if ( count( $country['city'] ) > 0 )
											<option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="col-md-2">
								<select id="starting-point" name="city" class="select-city">
									<option value="#">Starting Point</option>
								</select>
							</div>
							<div class="col-md-2">
								<select id="number-of-travellers" name="travellers">
									<option value="">Travellers</option>
									<option value="1" {{ session()->get( 'search_input' )['travellers'] == "1" ? 'selected' : '' }}>1</option>
									<option value="2" {{ session()->get( 'search_input' )['travellers'] == "2" ? 'selected' : '' }}>2</option>
									<option value="3" {{ session()->get( 'search_input' )['travellers'] == "3" ? 'selected' : '' }}>3</option>
									<option value="4" {{ session()->get( 'search_input' )['travellers'] == "4" ? 'selected' : '' }}>4</option>
									<option value="5" {{ session()->get( 'search_input' )['travellers'] == "5" ? 'selected' : '' }}>5</option>
									<option value="6" {{ session()->get( 'search_input' )['travellers'] == "6" ? 'selected' : '' }}>6</option>
									<option value="7" {{ session()->get( 'search_input' )['travellers'] == "7" ? 'selected' : '' }}>7</option>
									<option value="8" {{ session()->get( 'search_input' )['travellers'] == "8" ? 'selected' : '' }}>8</option>
									<option value="9" {{ session()->get( 'search_input' )['travellers'] == "9" ? 'selected' : '' }}>9</option>
									<option value="10" {{ session()->get( 'search_input' )['travellers'] == "10" ? 'selected' : '' }}>10</option>
								</select>
							</div>
							<div class="col-md-2">
								<input type="text" name="start_date" id="date-of-travel" placeholder="Travel Date" value="{{ session()->get( 'search_input' )['start_date'] }}">
							</div>
							<div class="col-md-4">
								<a href="#" class="update-search-btn padding"><i class="fa fa-search"></i> update</a>
							</div>
						</div>
						<div class="row content-header {{ session()->get( 'search_input' )['option'] == 'manual' ? 'hide' : '' }}" id="auto-container">
							<div class="col-md-2">
								<select id="to-country" name="to_country" class="select-country">
									<option value="#">Destination Country</option>
									@foreach ( $countries as $country )
										@if ( count( $country['city'] ) > 0 )
											<option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="col-md-2">
								<select id="to-city" name="to_city" class="select-city">
									<option value="#">Destination</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="submit" id="submit-btn" class="hide">
			</form>
		@endif --}}

		<!-- booking summary -->	
		<div class="page-sidebar-wrapper">
      		<div class="page-sidebar top-margin">
      			<div class="pageSidebar-inner">
					<div class="booking-summary" style="">
						<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>
					</div>
				</div>
			</div>
		</div>
		<!-- booking summary -->

		<div class="page-content-wrapper">
      		<div class="page-content top-margin">

				<div class="left-strip accomodationleftstrip">
					 <div class="leftStrip-icons">
						<?php 
							$function = Request::segment(1);
						    if($function == "map"){ 
						?>
							    @if ( session()->get( 'map_data' )['type'] == 'auto' )
							      	<p class="map-icon">
								        <a href="#helpBoxDiv" id="edit-map-btn">
									        <i class="icon icon-edit" ></i>
								        </a>
							      	</p>
							      	
								@else
								    <p class="map-icon">
								        <a href="#helpBoxDiv" id="edit-map-btn">
									        <i class="fa fa-question-circle" id="edit-map-btn"></i>
								        </a>
								    </p>
								    
								@endif
						<?php } else { ?>
						    <p class="map-icon">
						        <a href="/map">
						            <i class="icon icon-map">
						           </i>
						        </a>
						    </p>
						    
						<?php } ?>  
					</div>
					<a href="#" class="arrow-btn-new open"><i class="fa fa-angle-left"></i></a>
				</div>
            	<?php if($function == "map"){?>
            		<div class="map-main-container">
		            	<div id="map" style="height: 1000px;"></div>
			            <div class="mapWrapper">
			            	@yield( 'content' )
			            </div>
		            </div>
				<?php } else { ?>
					<div class="tabs-main-container accomodation-tabs-main-container">
			            <div id="map1">
			            	@yield( 'content' )
			            </div>
		        	</div>
				<?php } ?>
	        </div>
		</div>
	</div>
	<div class="clearfix"> </div>
	@yield('custom-css')
@include( 'layouts.partials.search_footer' )


@push('script')
<script type="text/javascript" src="{{ url('js/common.js')}}"></script>

@endpush
